import axios from '../axios'
const state = {
  user: null
}

const getters = {
  user(state) { return state.user }
}

const mutations = {
  setUser(state, user) {
    state.user = user
  }
}

const actions = {
  async registerUser({ commit }, { username, password }) {
    const newUser = await axios.post('/user/register', { username, password }).then(r => r.data)

    commit('setUser', newUser)
  },
  async loginUser({ commit }, { username, password }) {
    const user = await axios.post('/user/login', { username, password }).then(r => r.data)

    commit('setUser', user)
  },
  async logoutUser({ commit, state }) {
    const response = await axios.post('/user/logout', { userId: state.user.id })

    console.log('Logging out user', response, axios)
    commit('setUser', null)
  }
}

export default { state, getters, mutations, actions }
