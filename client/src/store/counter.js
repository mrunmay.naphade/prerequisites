const state = {
  tweets: 0,
}

const getters = {
  allTweets(state) { return state.tweets }
}

const mutations = {
  addTweet(state, value) {
    console.log('value', value)
    return state.tweets++
  }
}

const actions = {
  addTweetAction(arg1, arg2, arg3) {
    console.log('argsss', {arg1, arg2, arg3})
    arg1.commit('addTweet', 400)
  }
}

export default { state, getters, mutations, actions }
