import Vue from 'vue'
import Vuex from 'vuex'
import counter from './counter'
import tweets from './tweets'
import user from './user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    counter,
    tweets,
    user,
  }
})
