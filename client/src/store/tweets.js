import axios from '../axios'
const state = {
  tweets: [],
  ownedTweets: [],
}

const getters = {
  tweets(state) { return state.tweets },
  ownTweets(state) { return state.ownedTweets },
}

const mutations = {
  commitTweets(state, tweets) {
    state.tweets = tweets
  },
  commitOwnTweets(state, tweets) {
    state.ownedTweets = tweets
  }
}

const actions = {
  async fetchAllTweets({ commit }) {
    const tweets = await axios.get('/orm/tweets').then(r => r.data)
    commit('commitTweets', tweets)
  },
  addTweetAction(arg1, arg2, arg3) {
    console.log('argsss', {arg1, arg2, arg3})
    arg1.commit('addTweet', 400)
  },
  async postTweet({ dispatch }, { tweet, userId }) {
    const newTweet = await axios.post('/orm/tweet', { tweet, userId }).then(r => r.data)

    console.log('newtweet::::', newTweet, dispatch)
    dispatch('fetchAllTweets')
  },
  async fetchNewTweets({ commit }, userId) {
    const data = await axios.get(`/orm/latest/${userId}`).then(r => r.data).catch(e => console.log(e, 'in get new'))

    console.log('data:::', data, commit)
  },
  async fetchOwnTweets({ commit }, userId) {
    const data = await axios.get(`/orm/my-tweets/${userId}`).then(r => r.data).catch(e => console.log(e, 'in get new'))

    commit('commitOwnTweets', data)
  }
}

export default { state, getters, mutations, actions }
