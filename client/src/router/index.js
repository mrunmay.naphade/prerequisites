import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import NewTweet from '../views/NewTweet.vue'
import OwnTweets from '../views/OwnTweets.vue'
import Register from '../views/Register.vue'
import Tweet from '../components/Tweet'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/tweet/:id',
    name: 'Tweet',
    component: Tweet,
  },
  {
    path: '/my-tweets',
    name: 'NewTweet',
    component: OwnTweets,
  },
  {
    path: '/new-tweet',
    name: 'NewTweet',
    component: NewTweet,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
