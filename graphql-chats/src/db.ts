import { Model } from 'objection'
import knex from 'knex'

const createMaster = function(){
    return knex({
        client: 'pg',
        connection: {
            host: process.env.HOST,
            user: process.env.USERNAME,
            password: process.env.PASSWORD,
            database: process.env.DB
        },
        pool: { min: 2, max: 30 ,idleTimeoutMillis:300000 },
        searchPath: [process.env.NODE_ENV == "production" ? process.env.PROD_SCHEMA as string : process.env.DEV_SCHEMA as string]
    });    
}
export const knexMaster = createMaster();

const knexRead = knex({
    client: 'pg',
    connection: {
        host: process.env.READ_REPLICA_HOST,
        user: process.env.READ_REPLICA_USERNAME,
        password: process.env.READ_REPLICA_PASSWORD,
        database: process.env.READ_REPLICA_DB
    },
    pool: { min: 2, max: 30 },
    searchPath: [process.env.NODE_ENV == "production" ? process.env.PROD_SCHEMA as string : process.env.DEV_SCHEMA as string]
});

export const knexWrapper = createMaster();

knexWrapper.client.runner = function (builder: any) {

    if (process.env.USE_REPLICA == "false")
        return knexMaster.client.runner(builder);
    else if (this._is_master === true)
        return knexMaster.client.runner(builder);
    else {
        if (builder._method == "select")
            return knexRead.client.runner(builder);
        else
            return knexMaster.client.runner(builder);
    }
};

console.log(process.env.NODE_ENV == "production" ? process.env.DB : process.env.DB)
console.log(process.env.NODE_ENV == "production" ? process.env.PROD_SCHEMA : process.env.DEV_SCHEMA)

Model.knex(knexWrapper);

export default Model
