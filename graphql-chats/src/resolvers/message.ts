import ChatMessages from '../models/ChatMessagesModel'
import { pubsub } from '../server'

type MessageInputType = { message: {
    message: string,
    senderId: number,
    parentId: number | undefined
    chatRoomId: number
} }

export default {
    Query: {
        chatMessages(parent: any, args: any, context: any) {
            return ChatMessages.query()
                .where('chat_room_id', args.chatRoomId)
                .orderBy('created_at', 'DESC')
                .limit(10)
        }
    },
    Mutation: {
        async sendMessage(_parent: unknown, args: MessageInputType) {
            const { message: messageInput } = args

            const newMessage = await ChatMessages
                .query()
                .insert({
                    message: messageInput.message,
                    sender_id: messageInput.senderId,
                    parent_id: messageInput.parentId,
                    chat_room_id: messageInput.chatRoomId,
                    created_at: 'now()',
                    updated_at: 'now()',
                })
                .returning('*')

            pubsub.publish(`${messageInput.chatRoomId}_NEW_MESSAGE_CREATED`, newMessage)

            return newMessage
        }
    },
    Subscription: {
        newMessage: {
            resolve(args: any) {
                return args
            },
            subscribe: (parent: any, args: any) => {
                return pubsub.asyncIterator([`${args.chatRoomId}_NEW_MESSAGE_CREATED`]);
            }
        },
    },
}
