import BaseModel from './BaseModel'

export default class ChatsConversationsModel extends BaseModel {
    static get tableName() {
        return 'yocket_chats.chats_conversations';
    };

    static get idColumn() {
        return ['chat_room_id','logged_in_user_id'];
    };
}
