import BaseModel from './BaseModel'

export default class ChatUsersModel extends BaseModel {

    static get tableName() {
        return 'yocket_chats.chat_users';
    };

    static get idColumn() {
        return ['chat_room_id','user_id'];
    };

    static get relationMappings() {
        return {
            chat_room:{
                relation: BaseModel.HasOneRelation,
                modelClass: 'ChatRoomsModel',
                join: {
                    from: 'yocket_chats.chat_users.chat_room_id',
                    to: 'yocket_chats.chat_rooms.id'
                }
            },
            user:{
                relation: BaseModel.HasOneRelation,
                modelClass: 'UserModel',
                join: {
                    from: 'yocket_chats.chat_users.user_id',
                    to: 'users.id'
                }
            }
        }
    }
}
