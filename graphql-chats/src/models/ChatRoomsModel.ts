import BaseModel from './BaseModel'

export default class ChatRoomsModel extends BaseModel {

    static get tableName() {
        return 'yocket_chats.chat_rooms';
    };

    static get idColumn() {
        return 'id';
    };

    static get relationMappings() {
        return {
            chat_users:{
                relation: BaseModel.HasManyRelation,
                modelClass: 'ChatUsersModel',
                join: {
                    from: 'yocket_chats.chat_rooms.id',
                    to: 'yocket_chats.chat_users.chat_room_id'
                }
            }
        }
    }
}
