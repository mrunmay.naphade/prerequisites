import path from 'path'
import Model from '../db'

export default class BaseModel extends Model {
    static get modelPaths() {
        return [path.normalize(__dirname + '../../')];
    }
}
