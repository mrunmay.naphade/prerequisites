import BaseModel from './BaseModel'

export default class UserModel extends BaseModel {

    static get tableName() {
        return 'users';
    };

    static get idColumn() {
        return 'id';
    };
}
