import BaseModel from './BaseModel'

export default class MessagesModel extends BaseModel {

    static get tableName() {
        return 'yocket_chats.messages';
    };

    static get idColumn() {
        return 'id';
    };

    static get relationMappings() {
        return {
            chat_room:{
                relation: BaseModel.HasManyRelation,
                modelClass: 'ChatRoomsModel',
                join: {
                    from: 'yocket_chats.messages.chat_room_id',
                    to: 'yocket_chats.chat_rooms.id'
                }
            }
        }
    }
}
