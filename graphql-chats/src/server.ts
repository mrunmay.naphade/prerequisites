import { ApolloServer } from 'apollo-server-express';
import type { Config } from 'apollo-server-express';
import express from 'express';
import http from 'http';
import type { DocumentNode } from 'graphql';
import { execute, subscribe } from 'graphql';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { PubSub } from 'graphql-subscriptions';
import expressPlayground from 'graphql-playground-middleware-express'

import resolvers from './resolvers'
import typeDefs from './typedefs'

export const pubsub = new PubSub();

export default async function startApolloServer() {
  const app = express();
  const httpServer = http.createServer(app);
  const schema = makeExecutableSchema({ typeDefs, resolvers });

  const subscriptionServer = SubscriptionServer.create({
      // This is the `schema` we just created.
      schema,
      // These are imported from `graphql`.
      execute,
      subscribe,
  }, {
      // This is the `httpServer` we created in a previous step.
      server: httpServer,
      // Pass a different path here if your ApolloServer serves at
      // a different path.
      path: '/graphql',
  })

  const server = new ApolloServer({
      schema,
      plugins: [{
          async serverWillStart() {
              return {
                  async drainServer() {
                      subscriptionServer.close();
                  }
              };
          }
      }],
  } as Config);

  await server.start();
  server.applyMiddleware({ app });

  app.get('/playground', expressPlayground({ endpoint: '/graphql' }))

  await new Promise<void>(resolve => httpServer.listen({ port: 4000 }, resolve));
  console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}\n Visit playground on http://localhost:4000/playground`);
}
