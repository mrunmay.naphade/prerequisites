import { gql } from 'apollo-server'

export default gql`
type ChatRoom {
    blocked_by: String #uuid
    chat_users: [ChatUser!]!
        #distinct_on: [yocket_chats_chat_users_select_column!]limit: Intoffset: Intorder_by: [yocket_chats_chat_users_order_by!]where: yocket_chats_chat_users_bool_exp
    created_at: String #timestamptz!
    description: String
    display_picture_url: String
    firebase_coversation_id: String
    firebase_document(path: String): String #jsonb
    global_resource_id: String #uuid!
    id: Int!
    is_active: Boolean!
    is_group: Boolean!
    is_official: Boolean!
    is_private: Boolean!
    is_room_blocked: Boolean!
    label: String
    last_message_id: Int
    level: Int
    member_count: Int
    messages: [Message!]!
        #distinct_on: [yocket_chats_messages_select_column!]limit: Intoffset: Intorder_by: [yocket_chats_messages_order_by!]where: yocket_chats_messages_bool_exp

    name: String
    owner_id: Int
    premium_user_id: Int
    spam_count: Int
    term: String
    university_id: Int
    year: Int
}
`
