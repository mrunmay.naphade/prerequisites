import { gql } from 'apollo-server'

export default gql`
type User {
    created_at: String #timestamp
    first_name: String
    id: Int!
    is_blocked: Boolean!
    is_deleted: Boolean!
    last_logged_at: String #timestamp
    last_name: String
    level: Int
    name: String
    primary_email: String
    primary_phone: String
    role_id: Int
    service_status: Int
    session_id: String
    signup_complete: Boolean
    spam_count: Int
    term: String!
    updated_at: String #timestamp
    updated_by: Int
    username: String
    uuid: String #uuid!
    year: Int
}
`
