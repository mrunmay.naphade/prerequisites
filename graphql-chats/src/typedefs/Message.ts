import { gql } from 'apollo-server'

export default gql`
type Message {
    chat_room_id: Int!
    created_at: String #timestamptz
    data(path: String): String #jsonb
    firebase_conversation_id: String
    firebase_document(path: String): String #jsonb
    firebase_message_id: String
    firebase_parent_id: String
    global_resource_id: String #uuid!
    id: Int!
    id_new: Int
    is_deleted: Boolean!
    is_spam: Boolean
    message: String
    message_sender: User
    parent_id: Int
    parent_message: Message
    sender_id: Int
    sender_uuid: String #uuid
    sender_uuid_bkp: String
    spam_count: Int
    updated_at: String #timestamptz
}

extend type Query {
    chatMessages(chatRoomId: Int!): [Message!]!
}

input MessageInput {
    message: String!
    chatRoomId: Int!
    senderId: Int!
    parentId: Int
}

extend type Mutation {
    sendMessage(message: MessageInput!): Message!
}

extend type Subscription {
    newMessage(chatRoomId: Int!): Message!
}
`
