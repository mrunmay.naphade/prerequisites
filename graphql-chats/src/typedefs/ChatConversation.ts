import { gql } from 'apollo-server'

export default gql`
type ChatConversation {
    activity_type: String
    chat_room: ChatRoom
    chat_room_id: Int
    created_at: String #timestamptz
    is_message_deleted: Boolean
    is_mute: Boolean
    is_pinned: Boolean
    last_message: String
    last_message_data(path: String): String #jsonb
    last_message_id: Int
    last_message_timestamp: String #timestamptz
    logged_in_user_id: Int
    message: Message
    sender_first_name: String
    sender_id: Int
    unread_count: Int
    updated_at: String #timestamptz
}

extend type Query {
    chatConversation: ChatConversation
}
`
