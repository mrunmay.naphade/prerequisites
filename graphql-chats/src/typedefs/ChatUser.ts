import { gql } from 'apollo-server'

export default gql`
type ChatUser {
    chat_room_id: Int!
    chat_users: [User!]!
    #    distinct_on: [yocket_main_users_select_column!]limit: Intoffset: Intorder_by: [yocket_main_users_order_by!]where: yocket_main_users_bool_exp
    created_at: String #timestamptz
    is_admin: Boolean!
    is_mute: Boolean
    is_pinned: Boolean!
    last_opened: String #timestamptz
    last_read_message_id: Int
    last_read_timestamp: String #timestamptz
    last_typed: String #timestamptz
    user: User!
    user_id: Int!
    uuid: String #uuid
}
`
