import { gql } from 'apollo-server'
import ChatConversation from './ChatConversation'
import ChatRoom from './ChatRoom'
import ChatUser from './ChatUser'
import Message from './Message'
import User from './User'

const rootType = gql`
type Query {
    _dummy: String
}

type Mutation {
    _empty: String
}
type Subscription {
    _empty: String
}
`

export default [
    ChatConversation,
    ChatRoom,
    ChatUser,
    Message,
    User,
    rootType
]
