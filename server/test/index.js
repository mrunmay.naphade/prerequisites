const assert = require('assert')
const supertest = require('supertest')
const mockDb = require('mock-knex')

const { knex, app } = require('../src/server')

mockDb.mock(knex)

const server = supertest(app)

const { findById } = require('../src/dao/index')

function add(a, b) {
  return a + b
}
describe('Sample', () => {
  it('should pass', () => {
    const result = add(1,2)

    assert(3 === result)
  })
})

describe('server', () => {
  it('orm route', async () => {
    const res = await server.get('/orm');

    assert.equal(res.status,200);
  })
  it('login', async (done) => {
    const res = await server.post(
      '/user/login',
    ).send({ username: 'sudo', password: 'sudo' })

    assert.equal(res.status,200);
    assert.equal(res.body, {})

    done()
  })
})

describe.only('db', () => {
  it('find user', async () => {
    const tracker = mockDb.getTracker()
    tracker.install()
    tracker.on('query', function checkResult(query) {
      console.log(
        'query here:::', query
      )
      assert(query.method === 'select');
      assert.equal('sudo', query.bindings[0])
      query.response([
        {
          id : 'test-uuid',
          username : 'sudo'
        },
      ]);
    });

    const user = await findById('sudo', 'sudo')

    assert(user)
    assert.equal('sudo', user.username)
  })
})
