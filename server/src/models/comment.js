const { Model } = require('objection')

class Comment extends Model {
  static tableName() {
    return 'comments'
  }

  static relationMappings() {
    const Tweet = require('./tweet')
    return {
      tweet: {
        relation: Model.BelongsToOneRelation,
        modelClass: Tweet,
        join: {
          from: 'comments.tweet_id',
          to: 'tweets.id',
        }
      }
    }
  }
  
}

module.exports = Comment
