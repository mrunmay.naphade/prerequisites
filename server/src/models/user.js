const { Model } = require('objection')

class User extends Model {
  static tableName() {
    return 'users'
  }

  static relationMappings() {
    const Tweet = require('./tweet')
    return {
      tweets: {
        relation: Model.HasManyRelation,
        modelClass: Tweet,
        join: {
          from: 'users.id',
          to: 'tweets.created_by',
        }
      }
    }
  }
}

module.exports = User

