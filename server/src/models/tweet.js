const { Model } = require('objection')
const Comment = require('./comment')
const User = require('./user')

class Tweet extends Model {
  static tableName() {
    return 'tweets'
  }

  static relationMappings() {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: 'tweets.created_by',
          to: 'users.id',
        }
      },
      comments: {
        relation: Model.HasManyRelation,
        modelClass: Comment,
        join: {
          to: 'tweets.id',
          from: 'comments.tweet_id',
        }
      }
    }
  }

  static modifiers = {
      onlyNew(builder) {
        const today = new Date();
        today.setHours(0)
        return builder.where('created_at', '>=', today)
    }
  }
  
  
}

module.exports = Tweet
