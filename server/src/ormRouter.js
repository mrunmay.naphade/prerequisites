const { Router } = require('express')
const { v4 } = require('uuid')
const Tweet = require('./models/tweet')
const User = require('./models/user')
const Comment = require('./models/comment')

const router = Router()

router.get('/', (req, res) => {
  res.send('This is orm router working!')
})

router.get('/tweets', async (_req, res) => {
  const tweets = await Tweet
    .query()
    .withGraphFetched('user')
    .orderBy('created_at', 'DESC')
    .limit(20)

  res.send(tweets)
})

router.get('/my-tweets/:userId', async (req, res) => {
  const { userId } = req.params
  const tweets = await Tweet
    .query()
    .withGraphJoined('user')
    .where({ created_by: userId })
    .orderBy('created_at', 'DESC')
    .limit(20)

  res.send(tweets)
})

router.get('/latest/:userId', async (req, res) => {
  const { userId } = req.params

  const userWithTweets = await User.query()
    .findById(userId)
    .withGraphFetched('tweets')
    .modifyGraph('tweets', 'onlyNew')

  res.send(userWithTweets)
})

router.get('/comments', async (_req, res) => {
  const comments = await Comment.query().withGraphFetched('tweet')

  res.send(comments)
})

router.post('/tweet', async (req, res) => {
  const { tweet: desc, userId } = req.body
  
  if (!(desc && userId)) {
    return res.status(400).send('Invalid values')
  }

  const tweet = await Tweet.query().insert({
    id: Math.floor(Math.random() * 100000),
    tweet: desc,
    created_at: new Date(),
    created_by: userId,
  })

  res.send(tweet)
})

router.get('/addcomment', async (_, res) => {
  const tweet = await Comment.query().insert({
    id: v4(),
    comment: 'Second comment here',
    tweet_id: 1,
    created_at: new Date(),
  })

  res.send(tweet)
})

module.exports = router
