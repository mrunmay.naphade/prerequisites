const { createHmac } = require('crypto')

function hash(data) {
  const hmac = createHmac('md5', 'my super secret')

  hmac.update(data)
  return hmac.digest('base64')
}

module.exports = { hash }
