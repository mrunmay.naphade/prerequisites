const Knex = require('knex')
const { Model } = require('objection')
const User = require('../models/user')
const { hash } = require('../helpers')

const knex = Knex({
  client: 'pg',
  connection: {
    host: 'localhost',
    user: 'postgres',
    password: 'postgres',
    database: 'twitter',
  }
})

Model.knex(knex)

async function migrate() {
  const users = await User.query()

  const hashedPasswordsById = users.map(u => ({
    id: u.id,
    password: hash(u.password),
  }))

  const tableString = hashedPasswordsById.map(d => `
      select (\'${d.id}\')::uuid as id, \'${d.password}\' as password
    `)
    .join(' union ')

  console.log('BIG LOG', tableString)
  await User.knex().raw(`
    UPDATE users u
    SET password = data.password
    from (${tableString}) as data
    WHERE u.id = data.id
    `).then(r => console.log(r, "Scuess")).catch(console.log)
}

console.log('RUNNING MIGRAE')
migrate()
