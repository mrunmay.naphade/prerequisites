const express = require("express")
const cors = require('cors')
const bodyParser = require('body-parser')
const { Model } = require('objection')
const Knex = require('knex')
const ormRouter = require('./ormRouter')
const userRouter = require('./userRouter')

const knex = Knex({
  client: 'pg',
  connection: {
    host: 'localhost',
    user: 'postgres',
    password: 'postgres',
    database: 'twitter',
  }
})

Model.knex(knex)

const server = express()

server.use(cors())
server.use(bodyParser.json())
server.use('/orm', ormRouter)
server.use('/user', userRouter)

module.exports = { server, knex }
