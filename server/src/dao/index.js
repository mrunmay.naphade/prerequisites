const { hash } = require('../helpers')
const User = require('../models/user')

async function findById(username, password) {
  console.log('in the test:::', username, password)
  const user = await User.query().findOne({
    username, password: hash(password)
  }).select(['id', 'username'])

  console.log('user in dao:::', user)

  return user
}

module.exports = { findById }
