const { Router } = require('express')
const { hash } = require('./helpers')
const User = require('./models/user')

const router = Router()

router.post('/login', async (req, res) => {
  const { username, password } = req.body
  console.log("in login:::::", req.body, req.headers)

  const user = await User.query().findOne({
    username, password: hash(password)
  }).select(['id', 'username'])

  if(!user) {
    return res.status(400).send('User not found')
  }
  console.log('USER HERE::::', user)

  return res.send(user)
})

router.post('/logout', (req, res) => {
  const { userId } = req.body
  //do some cookie removal

  res.send('Logged Out')
})

router.post('/register', async (req,res) => {
  const { username, password } = req.body
  const hashedPassword = hash(password)

  const user = await User.query().insert({ username, password: hashedPassword })

  res.send({ id: user.id, username: user.username })
})

module.exports = router
